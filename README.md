# IA01 - TP03

**Problématique** : Quel est le snack du “Pic” qui correspond à votre envie du moment ?

Le système expert permet grâce à un système de chaînage avant de proposer le snack le plus adéquat à ce que veut manger l’utilisateur à un moment de la journée.
La base de fait contiendra donc tous les snacks disponibles au pic ainsi que leurs caractéristiques tel que sucré/salé, individuel /à partager, fruité/non fruité...
Le moteur d’inférence pourra se déplacer dans cette base en posant des questions afin d'affiner sa proposition pour l’utilisateur. 

# Base de Fait :

Ici Radin signifie souhaiter le moins cher. A l'inverse une personne qui n'est pas radine ne se souciera pas du prix.

```lisp
(setq snack 
    (list 
        'BatonDeBerger
        '1/3Baguette
        'Belin
        'Belvita
        'BiscuitSesame
        'Bounty
        'Bretzel
        'CacahuetesGrillees
        'ChaussonAuxPommes
        'ChipsNatenBIO
        'Cookie
        'CookiesChocoNoisetteNatenBIO
        'Croissant 
        'GaletteRizChoco
        'GaufreLiegeoise
        'Granola
        'GranyNoisettes
        'KinderBueno
        'KinderCountry
        'KitKat
        'Lion
        'M&M
        'Madeleine
        'Mars
        'MiniBriochePepitesDeChocolat
        'MiniSablesNatenBIO
        'Muffin
        'PailleDor
        'PainAuChocolat
        'PaletsCocoNatenBIO
        'Palmiers
        'PomPote
        'Pringles
        'SaucissonCharcUT
        'Snickers
        'TarteletteFramboiseNatenBIO
        'TUC
        'Twix
))

```

# Base de règles :

1. Sucré

- Si Sucré = 1 AND Heure > 8 AND Heure <= 11 ALORS **?PetitDejeune?**
- Si Sucré = 1 AND Heure > 11 ALORS **?Chocolat?**
- Si PetitDejeune = 0 ALORS **?Chocolat?**
- Si PetitDejeune = 1 AND Fruite = 1 ALORS *Chausson aux Pommes*
- Si PetitDejeune = 1 AND Fruite = 0 ALORS **?Viennoiserie?**
- Si Viennoiserie = 0 ALORS *Cookie*
- Si Viennoiserie = 1 AND Radin = 1 ALORS *Croissant*
- Si Viennoiserie = 1 AND Radin = 0 ALORS *Pain au Chocolatine* AND *Croissant*
- Si Chocolat = 1 AND Personne > 2 ALORS *Granola* AND *M&M*
- Si Chocolat = 1 AND Personne = 2 ALORS *Kinder Bueno* AND *Kit Kat* AND *Twix*
- Si Chocolat = 1 AND Personne = 1 AND Bio = 1 ALORS *Cookie Choco Noisette Naten Bio*
- Si Chocolat = 1 AND Personne = 1 AND Bio = 0 ALORS **?Barre?**
- Si Barre = 1 ALORS *Bounty* AND *Mars* AND *Snickers* AND *Lion*
- Si Barre = 0 AND Radin = 1 ALORS *Cookie* AND *Muffin*
- Si Barre = 0 AND Radin = 0 ALORS *Mini brioche Pepites de Chocolat* AND *Galette Riz Choco* AND *Grany Noisettes* AND *Kinder Country* AND *Cookie* AND *Muffin*
- Si Chocolat = 0 AND Personne >= 2 AND Fruite = 1 ALORS *Paille dor*
- Si Chocolat = 0 AND Personne >= 2 AND Fruite = 0 ALORS *Belvita* AND *Biscuit Aux sesame*
- Si Chocolat = 0 AND Personne = 1 AND Bio = 1 AND Radin = 1 ALORS *Tartelette Framboise Naten Bio*
- Si Chocolat = 0 AND Personne = 1 AND Bio = 1 AND Radin = 0 ALORS *Palets Coco Naten Bio* AND *Tartelette Framboise Naten Bio*
- Si Chocolat = 0 AND Personne = 1 AND Bio = 0 AND Fruite = 1 ALORS *PomPote*
- Si Chocolat = 0 AND Personne = 1 AND Bio = 0 AND Fruite = 0 ALORS *Gaufre Liegeoise* AND *Madeleine* AND *Palmiers*

2. Salé

- Si Sucré = 0 AND Personne = 2 ALORS *Bretzel*
- Si Sucré = 0 AND Personne = 1 ALORS **?Apero?**
- Si Apero = 1 ALORS *Baton de Berger*
- Si Apero = 0 ALORS *1/3 baguette*
- Si Sucré = 0 AND Personne > 2  AND Bio = 1 AND Radin = 1 ALORS *Chips Naten Bio*
- Si Sucré =0 AND Personne > 2 AND Bio = 1 AND Radin = 0 ALORS *Mini Sablé Naten Bio* AND *Chips Naten Bio*
-  Si Sucré =0 AND Personne > 2 AND Bio = 0 ALORS **?Charcuterie?**
- Si Charcuterie = 1 ALORS *Saucisson CharcUT*
- Si Charcuterie = 0 AND Radin = 1 ALORS *Tuc* AND *Belin* AND *Cacahuetes grillees*
- Si Charcuterie = 0 AND Radin = 0 ALORS *Pringles* AND *Tuc* AND *Belin* AND *Cacahuetes grillees*







<br>
<img src="/docs/arbre.png" alt="Arbre de décision"/>
<br>



