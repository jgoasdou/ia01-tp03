;; Système expert d'ordre 0+
;; Créé par GOASDOUÉ Justin & REDOUIN Noé
;; Dans le cadre de l'UV IA01 enseigné à l'Université de Technologie de Compiègne

(defun assoc2val (label base) ;;permet de renvoyer la velaur d'un attribut de la bdf
  (car(cdr (assoc label base))) 
)

(defun cmp (label op valeur)  ;;fonction permettant de simplifier les comparaison dans la bdr
  (let ((val (assoc2val label bdf)))
    (cond
      ((equal op '=) (eq val valeur))
      ((equal op '<=) (<= val valeur))
      ((equal op '>=) (>= val valeur))
      ((equal op '>) (> val valeur))
      ((equal op '<) (< val valeur))
    )
  )
)

(defun modify (sujet newval) ;crée une nouvelle liste avec nouvelle valeur pour un attribut de la bdf
  (let ((final nil))
    (dolist (X bdf final)
      (if (equal (car X) sujet) ;;lorsque l'on trouve l'atrribut
      (push (list sujet newval) final) ;initialisation de la nouvelle valeur   
      (push X final)
      )
    )
    (setq bdf final)  ;;mis à jour de la bdf
    ;;(format T "~& Nouvelle list ~a :   " final)
  )
)

(defun question (sujet) ;;fonction permettant de demander la valeur d'un attribut 
  (if (equal (assoc2val sujet bdf) -1)
    (let ((answer))
      (if (or (equal sujet 'personne) (equal sujet 'heure))
        (if (equal sujet 'personne) 
          (progn 
            (format T "~& Veuillez saisir le nombre de personnes (minimum 1) :   ");; attribut personne (comportement différent)
            (setq answer (read))
            (if (and (numberp answer) (> answer 0))
              (modify sujet answer)
              (progn
                (format T "~& La valeur n'est pas un nombre sup?rieur ? 0.   ")
                (question sujet)
              )  
            )
          )
          (progn ;;; attribut heure (comportement diférent)
            (format T "~& Veuillez saisir l heure (minimum 1) :   ")
            (setq answer (read))
            (if (and(and (numberp answer) (> answer 0)) (< answer 24)) 
              (modify sujet answer)
              (progn
                (format T "~& La valeur n'est pas un nombre entre 0 et 24.   ")
                (question sujet)
              )  
            )
          )
        )
        (progn ;;;autre atribut (0:non ou 1:oui)
          (format T "~& Veuillez saisir la nouvelle valeur de ~a (0 ou 1) :   " sujet)
          (setq answer (read))
          (if (and (numberp answer) (or (= answer 0) (= answer 1)))
            (modify sujet answer)
            (progn
              (format T "La valeur n'est pas 0 ou 1.   ")
              (question sujet)
            )  
          )
        )
      )
    )
    (format T "~& La valeur de ~a est deja renseignee" sujet)
  )
  T
)

(defun end (elements) ;focntion déclenché lorsque le SE a trouvé une solution
  (let ((result nil))
    (if (atom elements)
      (format T "~& Voici le(s) qui vous conviendra le mieux : ~a  "  elements)
      (dolist (X elements (format T "~& Voici le(s) qui vous conviendra le mieux : ~{~a~^, ~}  "  result))
        (push X result)
      )
    )
  )
  (defparameter stop 1);; Stop redéfini à 1 pour arrêter le moteur
)

(defun main (bdr)  ;;moteur de chainage avant récursif
  (let ((newbdr))
    (dolist (X bdr)
      (if (= stop 0)
        (if (eval (car (cdr x))) 
          (progn 
          ;;(format T "~& R�gle : ~a  "  X)  
          (setq newbdr (delete x bdr));; mis a jour de la bdr      
          ;;(format T "~& BDR : ~a  "  newbdr)
          (main newbdr) ;;appel récursif
          )
        )
      )
    )  
  )
)

(defun grandmain(bdr)
  (progn
    (reset) 
    (question 'heure) ;;on pose d'abord les questions sur les attributs nécessaires pour tous les chemins
    (question 'sucre)
    (question 'radin)
    (main bdr) ;;démarrage du moteur d'inférence
  )
)

(defun reset () ;; permet de réinitialisz stop à 0, la bdf et la bdr
  (defparameter stop 0)
  (setq bdf '((sucre -1) (heure -1) (petitdejeune -1) (viennoiserie -1) (personne -1) (chocolat -1) (bio -1) (fruite -1) (barre -1) (apero -1) (charcuterie -1) (radin -1)))
  (setq bdr 
    '(
      (R0 (if (not T) (princ 'false)))
      (R1 (if (and (cmp 'sucre '= 1) (cmp 'heure '<= 11)) (question 'petitdejeune)))
      (R2 (if (and (cmp 'sucre '= 1 ) (cmp 'heure '> 11)) (question 'chocolat)))
      (R3 (if (cmp 'petitdejeune '= 0) (question 'chocolat)))
      (R4 (if (cmp 'petitdejeune '= 1) (question 'fruite)))
      (R5 (if (not (cmp 'chocolat '= -1)) (question 'personne))) 
      (R6 (if (and (cmp 'petitdejeune '= 1)(cmp 'fruite '= 0)) (question 'viennoiserie) ))
      (R7 (if (and (and (cmp 'chocolat '= 1)(cmp 'personne '= 1))(cmp 'bio '= 0)) (question 'barre) ))
      (R8 (if (and (cmp 'chocolat '= 0) (cmp 'personne '>= 2)) (question 'fruite) ))
      (R9 (if (and (cmp 'chocolat '= 0) (cmp 'personne '= 1)) (question 'bio) ))
      (R10 (if (and(and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 0)) (question 'fruite)))
      (R11 (if (and (cmp 'chocolat '= 1) (cmp 'personne '= 1)) (question 'bio) ))
      (R12 (if (and (cmp 'petitdejeune '= 1)(cmp 'fruite '= 1)) (end 'chaussonaupomme) ))
      (R13 (if (cmp 'viennoiserie '= 0) (end 'cookie)))
      (R14 (if (and (cmp 'viennoiserie '= 1)(cmp 'radin '= 1)) (end 'croissant) ))
      (R15 (if (and (cmp 'viennoiserie '= 1)(cmp 'radin '= 0)) (end '(croissant painauchocolatine)) ))
      (R16 (if (and (cmp 'chocolat '= 1)(cmp 'personne '> 2)) (end '(granola mnm)) ))
      (R17 (if (and (cmp 'chocolat '= 1)(cmp 'personne '= 2)) (end '(kinderbueno kitkat twix)) ))
      (R18 (if (and (and (cmp 'chocolat '= 1)(cmp 'personne '= 1))(cmp 'bio '= 1)) (end 'cookiechoconoisettenatenbio) ))
      (R19 (if (cmp 'barre '= 1) (end '(bounty mars snickers lion))))
      (R20 (if (and (cmp 'barre '= 0)(cmp 'radin '= 1)) (end '(cookie muffin))))
      (R21 (if (and (cmp 'barre '= 0)(cmp 'radin '= 1)) 
              (end '(MinibriochepepitesdeChocolat GaletteRizChoco GranyNoisettes KinderCountry Cookie muffin))))      
      (R22 (if (and(and (cmp 'chocolat '= 0) (cmp 'personne '>= 2))(cmp 'fruite '= 1)) (end 'pailledor)))
      (R23 (if (and(and (cmp 'chocolat '= 0) (cmp 'personne '>= 2))(cmp 'fruite '= 0)) (end '(belvita biscuitauxsesame)) ))
      (R24 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 1)) (cmp 'radin '= 1)) (end 'TarteletteFramboiseNatenBio) ))
      (R25 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 1)) (cmp 'radin '= 0)) (end '(PaletsCocoNatenBio TarteletteFramboiseNatenBio) ) ))
      (R26 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 0)) (cmp 'fruite '= 1)) (end 'PomPote) ))
      (R27 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 0)) (cmp 'fruite '= 0)) (end '(GaufreLiegeoise Madeleine Palmiers)) ))
      (R28 (if (cmp 'sucre '= 0) (question 'personne)))
      (R29 (if (and (cmp 'sucre '= 0) (cmp 'personne '= 2)) (end 'Bretzel) ))
      (R30 (if (and (cmp 'sucre '= 0) (cmp 'personne '= 1)) (question 'apero)))
      (R31 (if (cmp 'apero '= 1) (end 'BatonDeBerger)))
      (R32 (if (cmp 'apero '= 0) (end 'Baguette)))
      (R33 (if (and (cmp 'sucre '= 0) (cmp 'personne '> 2)) (question 'bio)))
      (R34 (if (and (and (cmp 'sucre '= 0)(cmp 'personne '> 2)  )(and (cmp 'bio '= 1)(cmp 'radin '= 1))) (end 'ChipsNatenBio) ))
      (R35 (if (and (and (cmp 'sucre '= 0)(cmp 'personne '> 2)  )(and (cmp 'bio '= 1)(cmp 'radin '= 0))) (end '(MiniSableNatenBio ChipsNatenBio)) ))
      (R36 (if (and (and (cmp 'sucre '= 0)(cmp 'personne '> 2)  )(cmp 'bio '= 0)) (question 'charcuterie) ))
      (R37 (if (cmp 'charcuterie '= 1) (end 'saucissonCharcUT) ))
      (R38 (if (and (cmp 'charcuterie '= 0)(cmp 'radin '= 1)) (end '(Tuc Belin Cacahuetesgrillees))))
      (R39 (if (and (cmp 'charcuterie '= 0)(cmp 'radin '= 0)) (end '(Pringles Tuc Belin Cacahuetesgrillees))))
    )
  )
)

(reset)

;;; code à lancer pour faire focntionner le SE
(grandmain bdr)
