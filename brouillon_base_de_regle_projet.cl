(defun assoc2val (arg1 arg2) 
   (car(cdr (assoc arg1 arg2)))
)
(defun cmp (label op valeur)
  (let ((val (assoc2val label bdf)))
    ;;(if (equal val -1) (question label) )
    (cond
        ((equal op '=) (eq val valeur))
        ((equal op '<=) (<= val valeur))
        ((equal op '>=) (>= val valeur))
        ((equal op '>) (> val valeur))
        ((equal op '<) (< val valeur))
    )
  )
)

(setq bdr 
  '(
    (R1 (if (and (cmp 'sucre '= 1) (cmp 'heure '<= 11)) (question 'petitdejeune)))
    (R2 (if (and (cmp 'sucre '= 1 ) (cmp 'heure '> 11)) (question 'chocolat)))
    (R3 (if (cmp 'petitdejeune '= 0) (question 'chocolat)))
    (R3b (if (cmp 'petitdejeune '= 1) (question 'fruite)))
    (RClea (if (not (cmp 'chocolat '= -1)) (question 'personne))) 
    (R4 (if (and (cmp 'petitdejeune '= 1)(cmp 'fruite '= 1)) (end 'chaussonaupomme) ))
    (R5 (if (and (cmp 'petitdejeune '= 1)(cmp 'fruite '= 0)) (question 'viennoiserie) ))
    (R6 (if (cmp 'viennoiserie '= 0) (end 'cookie)))
    (R7 (if (and (cmp 'viennoiserie '= 1)(cmp 'radin '= 1)) (end 'croissant) ))
    (R8 (if (and (cmp 'viennoiserie '= 1)(cmp 'radin '= 0)) (end '(croissant painauchocolatine)) ))
    (R9 (if (and (cmp 'chocolat '= 1)(cmp 'personne '> 2)) (end '(granola mnm)) ))
    (R10 (if (and (cmp 'chocolat '= 1)(cmp 'personne '= 2)) (end '(kinderbueno kitkat twix)) ))
    (R11 (if (and (and (cmp 'chocolat '= 1)(cmp 'personne '= 1))(cmp 'bio '= 1)) (end 'cookiechoconoisettenatenbio) ))
    (R12 (if (and (and (cmp 'chocolat '= 1)(cmp 'personne '= 1))(cmp 'bio '= 0)) (question 'barre) ))
    (R13 (if (cmp 'barre '= 1) (end '(bounty mars snickers lion))))
    (R14 (if (and (cmp 'barre '= 0)(cmp 'radin '= 1)) (end '(cookie muffin))))
    (R15 (if (and (cmp 'barre '= 0)(cmp 'radin '= 1)) 
             (end '(MinibriochepepitesdeChocolat GaletteRizChoco GranyNoisettes KinderCountry Cookie muffin))))
    (R16a (if (and (cmp 'chocolat '= 0) (cmp 'personne '>= 2)) (question 'fruite) ))
    (R16b (if (and (cmp 'chocolat '= 0) (cmp 'personne '= 1)) (question 'bio) ))
    (R16c (if (and(and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 0)) (question 'fruite)))
    
    (R16 (if (and(and (cmp 'chocolat '= 0) (cmp 'personne '>= 2))(cmp 'fruite '= 1)) (end 'pailledor)))
    (R17 (if (and(and (cmp 'chocolat '= 0) (cmp 'personne '>= 2))(cmp 'fruite '= 0)) (end '(belvita biscuitauxsesame)) ))
    (R18 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 1)) (cmp 'radin '= 1)) (end 'TarteletteFramboiseNatenBio) ))
    (R19 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 1)) (cmp 'radin '= 0)) (end '(PaletsCocoNatenBio TarteletteFramboiseNatenBio) ) ))
    (R20 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 0)) (cmp 'fruite '= 1)) (end 'PomPote) ))
    (R21 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 0)) (cmp 'fruite '= 0)) (end '(GaufreLiegeoise Madeleine Palmiers)) ))
    
    (R22a (if (cmp 'sucre '= 0) (question 'personne)))
    (R22 (if (and (cmp 'sucre '= 0) (cmp 'personne '= 2)) (end 'Bretzel) ))
    (R23 (if (and (cmp 'sucre '= 0) (cmp 'personne '= 1)) (question 'apero)))
    (R24 (if (cmp 'apero '= 1) (end 'BatonDeBerger)))
    (R25 (if (cmp 'apero '= 0) (end 'Baguette)))
    (R23 (if (and (cmp 'sucre '= 0) (cmp 'personne '> 2)) (question 'bio)))
    (R26 (if (and (and (cmp 'sucre '= 0)(cmp 'personne '> 2)  )(and (cmp 'bio '= 1)(cmp 'radin '= 1))) (end 'ChipsNatenBio) ))
    (R27 (if (and (and (cmp 'sucre '= 0)(cmp 'personne '> 2)  )(and (cmp 'bio '= 1)(cmp 'radin '= 0))) (end '(MiniSableNatenBio ChipsNatenBio)) ))
    (R28 (if (and (and (cmp 'sucre '= 0)(cmp 'personne '> 2)  )(cmp 'bio '= 0)) (question 'charcuterie) ))
    (R29 (if (cmp 'charcuterie '= 1) (end 'saucissonCharcUT) ))
    (R30 (if (and (cmp 'charcuterie '= 0)(cmp 'radin '= 1)) (end '(Tuc Belin Cacahuetesgrillees))))
    (R31 (if (and (cmp 'charcuterie '= 0)(cmp 'radin '= 0)) (end '(Pringles Tuc Belin Cacahuetesgrillees))))
  )
)



(setq bdf '((sucre 0) (heure 8) (petitdejeune -1) (viennoiserie -1) (personne -1) (chocolat -1) (bio -1) (fruite -1) (barre -1) (apero -1) (charcuterie -1) (radin 0)))

(defun modify (sujet newval)
  (let ((final nil))
  (dolist (X bdf final)
    (if (equal (car X) sujet)
     (push (list sujet newval) final)    
     (push X final)
      )
    
    )
    (setq bdf final)
    (format T "~& Nouvelle list ~a :   " final)
  )
)

(defun question (sujet)
  (if (equal (assoc2val sujet bdf) -1)
    (let ((answer))
      (if (equal sujet 'personne)
         (progn 
           (format T "~& Veuillez saisir le nombre de personnes (minimum 1) :   ")
           (setq answer (read))
           (if (and (numberp answer) (> answer 0))
             (modify sujet answer)
             (progn
                (format T "~& La valeur n'est pas un nombre suprieur  0.   ")
                (question sujet)
             )  
           )
         )
        (progn 
           (format T "~& Veuillez saisir la nouvelle valeur de ~a (0 ou 1) :   " sujet)
           (setq answer (read))
           (if (and (numberp answer) (or (= answer 0) (= answer 1)))
             (modify sujet answer)
             (progn
                (format T "La valeur n'est pas 0 ou 1.   ")
                (question sujet)
             )  
           )
        )
      )
    )
    (format T "~& La valeur de ~a est deja renseignee" sujet)
  )
)


(defun end (elements) 
  (let ((result nil))
    (if (atom elements)
          (format T "~& Voici le(s) qui vous conviendra le mieux : ~a  "  elements)
        (dolist (X elements   (format T "~& Voici le(s) qui vous conviendra le mieux : ~{~a~^, ~}  "  result))
          (push X result)
          )
      )
    )
  (defparameter stop 1)
)



(defun main (bdr bdf)
  (defparameter stop 0)
  (let ((solution))
    (dolist (X bdr)
     (if (= stop 0)
     (eval (car (cdr x)))
     ;;(format T "s: ~&~a  "  x)
     )
      
    )  
  )
)







(defun reset ()
    
    (setq bdf '((sucre 1) (heure 8) (petitdejeune -1) (viennoiserie -1) (personne -1) (chocolat -1) (bio -1) (fruite -1) (barre -1) (apero -1) (charcuterie -1) (radin 1)))

    
)




(setq bdr (list
;;regle sucre
;Si Sucre = 1 AND Heure > 8 AND Heure <= 11 ALORS ?PetitDejeune? 
(R1 (eq (and (eq (eq (assoc 'sucre bdf) 1)T) (eq(eq (assoc 'sucre bdf) 1) T) T) (eq (<= (assoc 'heure bdf) 11) T) T) (Question bdf petitdejeune))
;Si Sucre = 1 AND Heure > 11 ALORS ?Chocolat? 
(R2 (eq (eq (eq (assoc 'sucre bdf) 1) T) (eq (> (assoc 'heure bdf) 11 ) T) T) (Question bdf chocolat))
;Si PetitDejeune = 0 ALORS ?Chocolat? 
(R3 (eq(eq (assoc 'petitdejeune bdf) 0) T) (Question bdf chocolat))
;R4 Si PetitDejeune = 1 AND Fruite = 1 ALORS Chausson aux Pommes 
(R4 (eq (eq(eq (assoc 'petitdejeune bdf) 1) T) (eq(eq (assoc 'fruite bdf) 1) T)T) '(chaussonaupomme))
;R5 Si PetitDejeune = 1 AND Fruite = 0 ALORS ?Viennoiserie?
(R5 (eq (eq(eq (assoc 'petitdejeune bdf) 1) T) (eq(eq (assoc 'fruite bdf) 0) T)T) (Question bdf viennoiserie))
;R6 Si Viennoiserie = 0 ALORS Cookie
(R6 (eq(eq (assoc 'Viennoiserie bdf) 0) T) '(cookie))
;R7 Si Viennoiserie = 1 AND Radin = 1 ALORS Croissant
(R7 (eq (eq (eq (assoc 'Viennoiserie bdf) 1)T) (eq(eq (assoc 'Radin bdf) 1)T)T) '(Croissant))
;R8 Si Viennoiserie = 1 AND Radin = 0 ALORS Pain au Chocolatine AND Croissant 
(R8 (eq (eq(eq (assoc 'viennoiserie bdf) 1)T) (eq(eq (assoc 'Radin bdf) 0)T)T) '(PainAuChocolatine Croissant))
;R9 Si Chocolat = 1 AND Personne > 2 ALORS Granola AND M&M 
(R9 (eq (eq (eq (assoc 'chocolat bdf) 1) T) (eq (> (assoc 'personne bdf) 2) T) T) '(granola mm))
;Si Chocolat = 1 AND Personne = 2 ALORS Kinder Bueno AND Kit Kat AND Twix 
(R10 (eq (eq(eq (assoc 'chocolat bdf) 1)T) (eq (eq (assoc 'personne bdf) 2) T) T) '(kinderbueno kitkat twix))
;Si Chocolat = 1 AND Personne = 1 AND Bio = 1 ALORS Cookie Choco Noisette Naten Bio 
(R11 (eq(eq (eq(eq (assoc 'chocolat bdf) 1)T) (eq (eq (assoc 'personne bdf) 1) T)) (eq(eq (assoc 'bio bdf) 1)T) T) '(CookiesChocoNoisetteNatenBIO))
;Si Chocolat = 1 AND Personne = 1 AND Bio = 0 ALORS ?Barre? 
(R11 (eq(eq (eq(eq (assoc 'chocolat bdf) 1)T) (eq (eq (assoc 'personne bdf) ) T)) (eq(eq (assoc 'bio bdf) 1)T) T) (question bdf barre))
;Si Barre = 1 ALORS Bounty AND Mars AND Snickers AND Lion 
(R12 (eq(eq (assoc 'barre bdf) 1) T) '(bounty mars snickers lion))
;Si Barre = 0 AND Radin = 1 ALORS Cookie AND Muffin
(R13 (eq (eq (eq (assoc 'barre bdf) 0)T) (eq(eq (assoc 'Radin bdf) 1)T)T) '(cookie muffin))
;Si Barre = 0 AND Radin = 0 ALORS Mini brioche Pepites de Chocolat AND Galette Riz Choco AND Grany Noisettes AND Kinder Country AND Cookie AND Muffin 
(R13 (eq (eq (eq (assoc 'barre bdf) 0)T) (eq(eq (assoc 'Radin bdf) 0)T)T) '(miniBriochePepiteDeChocolat galetteRizChoco granyNoisettes KinderCountry cookie Muffin))
;Si Chocolat = 0 AND Personne >= 2 AND Fruite = 1 ALORS Paille dor
(R14 (eq(eq (eq(eq (assoc 'chocolat bdf) 0)T) (eq (>= (assoc 'personne bdf) 2) T)) (eq(eq (assoc 'fruite bdf) 1)T) T) '(pailleDor))
;Si Chocolat = 0 AND Personne >= 2 AND Fruite = 0 ALORS Belvita AND Biscuit Aux sesame
(R15 (eq(eq (eq(eq (assoc 'chocolat bdf) 0)T) (eq (>= (assoc 'personne bdf) 2) T)) (eq(eq (assoc 'fruite bdf) 0)T) T) '(belvita biscuitAuSesame))
;Si Chocolat = 0 AND Personne = 1 AND Bio = 1 AND Radin = 1 ALORS Tartelette Framboise Naten Bio
(R16 (eq(eq(eq (eq(eq (assoc 'chocolat bdf) 0)T) (eq (eq (assoc 'personne bdf) 1) T) T) (eq(eq (assoc 'bio bdf) 1)T) T) (eq(eq (assoc 'radin bdf) 1)T) T) '(tarteletteFramboiseNatenBio))
;Si Chocolat = 0 AND Personne = 1 AND Bio = 1 AND Radin = 0 ALORS Palets Coco Naten Bio AND Tartelette Framboise Naten Bio 
(R17 (eq(eq(eq (eq(eq (assoc 'chocolat bdf) 0)T) (eq (eq (assoc 'personne bdf) 1) T) T) (eq(eq (assoc 'bio bdf) 1)T) T) (eq(eq (assoc 'radin bdf) 0)T) T) '(PaletCocoNatenBio tarteletteFramboiseNatenBio))
;Si Chocolat = 0 AND Personne = 1 AND Bio = 0 AND Fruite = 1 ALORS PomPote 
(R18 (eq(eq(eq (eq(eq (assoc 'chocolat bdf) 0)T) (eq (eq (assoc 'personne bdf) 1) T) T) (eq(eq (assoc 'bio bdf) 0)T) T) (eq(eq (assoc 'fruite bdf) 1)T) T) '(pompote))
;Si Chocolat = 0 AND Personne = 1 AND Bio = 0 AND Fruite = 0 ALORS Gaufre Liegeoise AND Madeleine AND Palmiers 
(R19 (eq(eq(eq (eq(eq (assoc 'chocolat bdf) 0)T) (eq (eq (assoc 'personne bdf) 1) T) T) (eq(eq (assoc 'bio bdf) 0)T) T) (eq(eq (assoc 'fruite bdf) 0)T) T) '(gaufreLiegeoise madeleine palmiers))




;regle sal
;Si Sucr = 0 AND Personne = 2 ALORS Bretzel 
(R20 (eq (eq (eq (assoc 'sucre bdf) 0) T) (eq (eq (assoc 'personne bdf) 2) T) T) '(bretzel))
;Si Sucr = 0 AND Personne = 1 ALORS ?Apero? 
(R21 (eq (eq (eq (assoc 'sucre bdf) 1) T) (eq (eq (assoc 'personne bdf) 1) T) T) (question bdf apero))
;Si Apero = 1 ALORS Baton de Berger 
(R22 (eq(eq (assoc 'apero bdf) 1) T) '(batonDeBerger))
;Si Apero = 0 ALORS 1/3 baguette 
(R23 (eq(eq (assoc 'apero bdf) 0) T) '(1/3Baguette))
;Si Sucr = 0 AND Personne > 2 AND Bio = 1 AND Radin = 1 ALORS Chips Naten Bio 
(R24 (eq(eq(eq (eq(eq (assoc 'sucre bdf) 0)T) (eq (> (assoc 'personne bdf) 2) T) T) (eq(eq (assoc 'bio bdf) 1)T) T) (eq(eq (assoc 'radin bdf) 1)T) T) '(chipsNatenBio))
;Si Sucr =0 AND Personne > 2 AND Bio = 1 AND Radin = 0 ALORS Mini Sabl Naten Bio AND Chips Naten Bio
(R25 (eq(eq(eq (eq(eq (assoc 'sucre bdf) 0)T) (eq (> (assoc 'personne bdf) 2) T) T) (eq(eq (assoc 'bio bdf) 1)T) T) (eq(eq (assoc 'radin bdf) 0)T) T) '(chipsNatenBio miniSableNatenBio))
;Si Sucr =0 AND Personne > 2 AND Bio = 0 ALORS ?Charcuterie? 
(R26 (eq(eq (eq(eq (assoc 'sucre bdf) 0)T) (eq (> (assoc 'personne bdf) 2) T)) (eq(eq (assoc 'bio bdf) 0)T) T) (question bdf charcuterie))
;Si Charcuterie = 1 ALORS Saucisson CharcUT 
(R27 (eq(eq (assoc 'charcuterie bdf) 1) T) '(saucissonCharcUT))
;Si Charcuterie = 0 AND Radin = 1 ALORS Tuc AND Belin AND Cacahuetes grillees 
(R28 (eq (eq (eq (assoc 'charcuterie bdf) 0) T) (eq (eq (assoc 'radin bdf) 1) T) T) '(Tuc Belin cacahuetesGrillees))
;Si Charcuterie = 0 AND Radin = 0 ALORS Pringles AND Tuc AND Belin AND Cacahuetes grillees 
(R29 (eq (eq (eq (assoc 'charcuterie bdf) 0) T) (eq (eq (assoc 'radin bdf) 0) T) T) '(Pringles Tuc Belin cacahuetesGrillees))
)
)

;;;;main version noe
(defun assoc2val (arg1 arg2) 
  (car(cdr (assoc arg1 arg2))) 
  )



(defun cmp (label op valeur)
  (let ((val (assoc2val label bdf)))
    ;;(if (equal val -1) (question label) )
    (cond
        ((equal op '=) (eq val valeur))
        ((equal op '<=) (<= val valeur))
        ((equal op '>=) (>= val valeur))
        ((equal op '>) (> val valeur))
        ((equal op '<) (< val valeur))
    )
  )
)

(defparameter stop 0)




(setq bdf '((sucre 1) (heure 8) (petitdejeune -1) (viennoiserie -1) (personne -1) (chocolat -1) (bio -1) (fruite -1) (barre -1) (apero -1) (charcuterie -1) (radin 0)))

(defun modify (sujet newval) ;crée une nouvelle liste avec nouvelle valeur pour un paramètre
  (let ((final nil))
  (dolist (X bdf final)
    (if (equal (car X) sujet)
     (push (list sujet newval) final) ;lorsque nouvelle valeur nécessaire  
     (push X final)
      )
    
    )
    (setq bdf final)
    (format T "~& Nouvelle list ~a :   " final)
  )
)

(defun question (sujet)
  (if (equal (assoc2val sujet bdf) -1)
    (let ((answer))
      (if (or (equal sujet 'personne) (equal sujet 'heure))
          (if (equal sujet 'personne) 
          (progn 
           (format T "~& Veuillez saisir le nombre de personnes (minimum 1) :   ")
           (setq answer (read))
           (if (and (numberp answer) (> answer 0))
             (modify sujet answer)
             (progn
                (format T "~& La valeur n'est pas un nombre sup?rieur ? 0.   ")
                (question sujet)
             )  
           ))
            (progn 
           (format T "~& Veuillez saisir l heure (minimum 1) :   ")
           (setq answer (read))
           (if (and(and (numberp answer) (> answer 0)) (< answer 24)) 
             (modify sujet answer)
             (progn
                (format T "~& La valeur n'est pas un nombre entre 0 et 24.   ")
                (question sujet)
             )  
             ))
            )
            
        (progn 
           (format T "~& Veuillez saisir la nouvelle valeur de ~a (0 ou 1) :   " sujet)
           (setq answer (read))
           (if (and (numberp answer) (or (= answer 0) (= answer 1)))
             (modify sujet answer)
             (progn
                (format T "La valeur n'est pas 0 ou 1.   ")
                (question sujet)
             )  
           )
        )
      )
    )
    (format T "~& La valeur de ~a est deja renseignee" sujet)
  )
  T
)


(defun end (elements) 
  (let ((result nil))
    (if (atom elements)
          (format T "~& Voici le(s) qui vous conviendra le mieux : ~a  "  elements)
        (dolist (X elements   (format T "~& Voici le(s) qui vous conviendra le mieux : ~{~a~^, ~}  "  result))
          (push X result)
          )
      )
    )
  (defparameter stop 1)
)


(defun main (bdr)
  (let ((newbdr))
    (dolist (X bdr)
      (if (= stop 0)
      (if (eval (car (cdr x))) 
          (progn 
           (format T "~& Règle : ~a  "  X)  
          (setq newbdr (remove x bdr))
                  
          (format T "~& BDR : ~a  "  newbdr)
          (main newbdr)
        )
      )
     ;;(format T "s: ~&~a  "  x)
     )
    )  
  )
)


(defun grandmain(bdr)
  (progn 
    (question 'heure)
    (question 'sucre)
    (question 'radin)
    (main bdr)
  ))



(defun reset ()
    (defparameter stop 0)
    (setq bdf '((sucre -1) (heure -1) (petitdejeune -1) (viennoiserie -1) (personne -1) (chocolat -1) (bio -1) (fruite -1) (barre -1) (apero -1) (charcuterie -1) (radin -1)))
    (setq bdr 
      '(
        (R1 (if (and (cmp 'sucre '= 1) (cmp 'heure '<= 11)) (question 'petitdejeune)))
        (R2 (if (and (cmp 'sucre '= 1 ) (cmp 'heure '> 11)) (question 'chocolat)))
        (R3 (if (cmp 'petitdejeune '= 0) (question 'chocolat)))
        (R3b (if (cmp 'petitdejeune '= 1) (question 'fruite)))
        (RClea (if (not (cmp 'chocolat '= -1)) (question 'personne))) 
        (R5 (if (and (cmp 'petitdejeune '= 1)(cmp 'fruite '= 0)) (question 'viennoiserie) ))
        (R12 (if (and (and (cmp 'chocolat '= 1)(cmp 'personne '= 1))(cmp 'bio '= 0)) (question 'barre) ))
        (R16a (if (and (cmp 'chocolat '= 0) (cmp 'personne '>= 2)) (question 'fruite) ))
        (R16b (if (and (cmp 'chocolat '= 0) (cmp 'personne '= 1)) (question 'bio) ))
        (R16c (if (and(and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 0)) (question 'fruite)))
        (R16d (if (and (cmp 'chocolat '= 1) (cmp 'personne '= 1)) (question 'bio) ))

        
        (R4 (if (and (cmp 'petitdejeune '= 1)(cmp 'fruite '= 1)) (end 'chaussonaupomme) ))
        
        (R6 (if (cmp 'viennoiserie '= 0) (end 'cookie)))
        (R7 (if (and (cmp 'viennoiserie '= 1)(cmp 'radin '= 1)) (end 'croissant) ))
        (R8 (if (and (cmp 'viennoiserie '= 1)(cmp 'radin '= 0)) (end '(croissant painauchocolatine)) ))
        (R9 (if (and (cmp 'chocolat '= 1)(cmp 'personne '> 2)) (end '(granola mnm)) ))
        (R10 (if (and (cmp 'chocolat '= 1)(cmp 'personne '= 2)) (end '(kinderbueno kitkat twix)) ))
        (R11 (if (and (and (cmp 'chocolat '= 1)(cmp 'personne '= 1))(cmp 'bio '= 1)) (end 'cookiechoconoisettenatenbio) ))
        
        (R13 (if (cmp 'barre '= 1) (end '(bounty mars snickers lion))))
        (R14 (if (and (cmp 'barre '= 0)(cmp 'radin '= 1)) (end '(cookie muffin))))
        (R15 (if (and (cmp 'barre '= 0)(cmp 'radin '= 1)) 
                (end '(MinibriochepepitesdeChocolat GaletteRizChoco GranyNoisettes KinderCountry Cookie muffin))))
     

                
                
        (R16 (if (and(and (cmp 'chocolat '= 0) (cmp 'personne '>= 2))(cmp 'fruite '= 1)) (end 'pailledor)))
        (R17 (if (and(and (cmp 'chocolat '= 0) (cmp 'personne '>= 2))(cmp 'fruite '= 0)) (end '(belvita biscuitauxsesame)) ))
        (R18 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 1)) (cmp 'radin '= 1)) (end 'TarteletteFramboiseNatenBio) ))
        (R19 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 1)) (cmp 'radin '= 0)) (end '(PaletsCocoNatenBio TarteletteFramboiseNatenBio) ) ))
        (R20 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 0)) (cmp 'fruite '= 1)) (end 'PomPote) ))
        (R21 (if (and (and (and (cmp 'chocolat '= 0) (cmp 'personne '= 1))(cmp 'bio '= 0)) (cmp 'fruite '= 0)) (end '(GaufreLiegeoise Madeleine Palmiers)) ))
        
        (R22a (if (cmp 'sucre '= 0) (question 'personne)))
        (R22 (if (and (cmp 'sucre '= 0) (cmp 'personne '= 2)) (end 'Bretzel) ))
        (R23 (if (and (cmp 'sucre '= 0) (cmp 'personne '= 1)) (question 'apero)))
        (R24 (if (cmp 'apero '= 1) (end 'BatonDeBerger)))
        (R25 (if (cmp 'apero '= 0) (end 'Baguette)))
        (R23 (if (and (cmp 'sucre '= 0) (cmp 'personne '> 2)) (question 'bio)))
        (R26 (if (and (and (cmp 'sucre '= 0)(cmp 'personne '> 2)  )(and (cmp 'bio '= 1)(cmp 'radin '= 1))) (end 'ChipsNatenBio) ))
        (R27 (if (and (and (cmp 'sucre '= 0)(cmp 'personne '> 2)  )(and (cmp 'bio '= 1)(cmp 'radin '= 0))) (end '(MiniSableNatenBio ChipsNatenBio)) ))
        (R28 (if (and (and (cmp 'sucre '= 0)(cmp 'personne '> 2)  )(cmp 'bio '= 0)) (question 'charcuterie) ))
        (R29 (if (cmp 'charcuterie '= 1) (end 'saucissonCharcUT) ))
        (R30 (if (and (cmp 'charcuterie '= 0)(cmp 'radin '= 1)) (end '(Tuc Belin Cacahuetesgrillees))))
        (R31 (if (and (cmp 'charcuterie '= 0)(cmp 'radin '= 0)) (end '(Pringles Tuc Belin Cacahuetesgrillees))))
      )
    )
)


(reset)

(grandmain bdr)
